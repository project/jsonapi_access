<?php

namespace Drupal\jsonapi_access\EventSubscriber;

use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\jsonapi\JsonApiResource\JsonApiDocumentTopLevel;
use Drupal\jsonapi\Query\Filter;
use Drupal\jsonapi\ResourceResponse;
use Drupal\jsonapi_extras\ResourceType\ConfigurableResourceType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ResponseSubscriber.
 */
class ResponseSubscriber implements EventSubscriberInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE] = ['onResponse', 512];

    return $events;
  }

  /**
   * This method is called the KernelEvents::RESPONSE event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The filter event.
   */
  public function onResponse(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }
    $request = $event->getRequest();
    $include_access = explode(',', $request->get('jsonapi_access', ''));
    if (!in_array('create', $include_access)) {
      return;
    }
    $response = $event->getResponse();
    if (!$response instanceof ResourceResponse) {
      return;
    }
    $route_match = $this->container->get('current_route_match');
    $resource_type = $route_match->getParameter('resource_type');
    if (!$resource_type instanceof ConfigurableResourceType) {
      return;
    }
    $em = $this->container->get('entity_type.manager');
    $access = $em->getAccessControlHandler($resource_type->getEntityTypeId());
    if (!$access instanceof EntityAccessControlHandlerInterface) {
      return;
    }
    $context[Filter::KEY_NAME] = $request->query->get(Filter::KEY_NAME);
    $meta['access'] = [];
    if ($access->createAccess($resource_type->getBundle(), NULL, $context)) {
      $meta['access'][] = 'create';
    }
    $event->setResponse($this->createResponse($response, $meta));
  }

  /**
   * Re-create json response with meta access.
   *
   * @param \Drupal\jsonapi\ResourceResponse $response
   *   Original response object.
   *
   * @param array $meta
   *   Additional meta information.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response with additional parameters or original if not applicable.
   */
  protected function createResponse(ResourceResponse $response, array $meta): Response {
    $responseData = $response->getResponseData();
    if (!$responseData instanceof JsonApiDocumentTopLevel) {
      return $response;
    }
    $data = $responseData->getData();
    $includes = $responseData->getIncludes();
    $links = $responseData->getLinks();
    $meta = $meta + $responseData->getMeta();

    $cacheMeta = $response->getCacheableMetadata();
    $cacheMeta->addCacheContexts(['url.query_args:jsonapi_access']);
    $newResponse = new ResourceResponse(
      new JsonApiDocumentTopLevel($data, $includes, $links, $meta),
      $response->getStatusCode(),
      $response->headers->all()
    );
    $newResponse->addCacheableDependency($cacheMeta);
    return $newResponse;
  }

}
