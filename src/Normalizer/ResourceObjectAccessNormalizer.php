<?php

namespace Drupal\jsonapi_access\Normalizer;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\jsonapi\JsonApiResource\ResourceObject;
use Drupal\jsonapi\Normalizer\Value\CacheableNormalization;
use Drupal\jsonapi\ResourceType\ResourceType;
use Drupal\jsonapi_extras\Normalizer\JsonApiNormalizerDecoratorBase;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Converts the Drupal entity reference item object to a JSON:API structure.
 */
class ResourceObjectAccessNormalizer extends JsonApiNormalizerDecoratorBase {

  /**
   * The Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * JsonApiNormalizerDecoratorBase constructor.
   *
   * @param \Symfony\Component\Serializer\SerializerAwareInterface|\Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $inner
   *   The decorated normalizer or denormalizer.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The Request stack.
   */
  public function __construct($inner, RequestStack $requestStack) {
    parent::__construct($inner);
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $include_access = explode(',', $this->requestStack->getCurrentRequest()->get('jsonapi_access'));
    // No sense to check create access per entity.
    $create = array_search('create', $include_access);
    if ($create !== FALSE) {
      unset($include_access[$create]);
    }
    assert($object instanceof ResourceObject);
    $resource_type = $object->getResourceType();
    $cacheable_normalization = parent::normalize($object, $format, $context);
    assert($cacheable_normalization instanceof CacheableNormalization);
    if (is_subclass_of($resource_type->getDeserializationTargetClass(), ContentEntityInterface::class)) {
      return (new CacheableNormalization(
        $cacheable_normalization,
        static::enhanceConfigFields($object, $cacheable_normalization->getNormalization(), $resource_type, $include_access)
      ))->withCacheableDependency((new CacheableMetadata())->setCacheContexts(['url.query_args:jsonapi_access']));
    }
    return $cacheable_normalization;
  }

  /**
   * Applies field enhancers to a config entity normalization.
   *
   * @param \Drupal\jsonapi\JsonApiResource\ResourceObject $resource
   *   The resource object.
   * @param array $normalization
   *   The normalization to be enhanced.
   * @param \Drupal\jsonapi\ResourceType\ResourceType $resource_type
   *   The resource type of the normalized resource object.
   * @param array $include_access
   *   List of access permissions to include.
   *
   * @return array
   *   The enhanced field data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function enhanceConfigFields(ResourceObject $resource, array $normalization, ResourceType $resource_type, array $include_access) {
    if (empty($include_access)) {
      return $normalization;
    }
    $entity_type_id = $resource_type->getEntityTypeId();
    $em = \Drupal::entityTypeManager();
    $uuid_key = $em->getDefinition($entity_type_id)->getKey('uuid');
    $storage = $em->getStorage($entity_type_id);
    if (!$storage) {
      return $normalization;
    }
    $entities = $storage->loadByProperties([$uuid_key => $resource->getId()]);
    if (!$entities) {
      return $normalization;
    }
    $entity = reset($entities);
    $access = $em->getAccessControlHandler($entity_type_id);
    if (!$access instanceof EntityAccessControlHandlerInterface) {
      return $normalization;
    }
    $normalization['meta']['access'] = [];
    foreach ($include_access as $action) {
      if ($action && $access->access($entity, $action)) {
        $normalization['meta']['access'][] = $action;
      }
    }
    return $normalization;
  }

}
