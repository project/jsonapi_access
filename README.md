CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides access checks for any entity operations in the JSON response.
Based on JSONAPI:Extras.

Must have module when you want to build fully decouple with access check.

Usage:

Add jsonapi_access=create,update,delete to your regular jsonapi request and
you will see "meta[access]" for general response and for each resource
inside it.

More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/jsonapi_access).

REQUIREMENTS
------------

This module requires jsonapi and jsonapi_extras outside of Drupal core.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

There is no configuration.

MAINTAINERS
-----------

Current maintainers:
* Alexandr Davyskiba (zviryatko) - https://www.drupal.org/u/zviryatko
